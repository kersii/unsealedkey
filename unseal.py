#!/usr/bin/env python3

import Crypto.Hash, sys
from binascii import hexlify
from Crypto.Cipher import AES

def memcmp(a, b, n, oa = 0, ob = 0):
	if (len(a) < n or len(b) < n):
		print('bad comparison length', file = sys.stderr)
		return False

	for i in range(n):
		ai = ord(a[i + oa]) if (isinstance(a[i + oa], str)) else a[i + oa]
		bi = ord(b[i + ob]) if (isinstance(b[i + ob], str)) else b[i + ob]

		if (ai != bi):
			return False

	return True

def readKey(keyName):
	with open(keyName, 'rb') as f:
		return f.read()

def checkKey(key):
	def checkHMAC(hmac, key):
		res = memcmp(hmac, key, 32, ob = 64)
		print('{} hmac'.format('good' if (res) else 'bad'))

		return res

	def sha256HMAC(key):
		hmacKey = readKey('savedatamasterhashkey{}.bin'.format(key[8]))
		hmac = Crypto.Hash.HMAC.new(hmacKey, msg = key[:64], digestmod = Crypto.Hash.SHA256)

		return hmac.digest()

	res = memcmp(key, 'pfsSKKey', 8)

	print('{} key'.format('good' if (res) else 'bad'))

	if (res):
		return checkHMAC(sha256HMAC(key), key)

	return res

def decryptKey(key):
	iv = key[16:32]
	encKey = key[32:64]
	aesKey = readKey('savedatamasterkey{}.bin'.format(key[8]))[:16]

	cipher = AES.new(aesKey, Crypto.Cipher.AES.MODE_CBC, iv)

	return cipher.decrypt(encKey)

def writeDecKey(key):
	with open('decKey.bin', 'wb') as dk:
		dk.write(key)
		print('wrote key to decKey.bin')

def main():
	if (len(sys.argv) < 2):
		print('must specify key file', file = sys.stderr)
		return

	key = readKey(sys.argv[1])
	if (not checkKey(key)):
		return

	decKey = decryptKey(key)
	print('decrypted pfs key: {}'.format(hexlify(decKey)))

	writeDecKey(decKey)

main()
